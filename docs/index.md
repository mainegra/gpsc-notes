Title:   My Document
Summary: A brief description of my document.
Authors: Ernesto Mainegra-Hing
         Tom Christie
Date:    January 23, 2018
blank-value:
some_url: https://example.com

# Government of Canada HPC environment
_Created by Ernesto Mainegra-Hing_

This document details the steps taken to set up the Government of Canada
HPC environment for an NRC Metrology user. It covers the experiences from
figuring out how to set up the user environment to implementing a job submission
strategy for the SGE batch-queuing system. To this end it was necessary to comb
through the rather scattered information on the Government of Canada Science Portal.
I have placed these notes here to keep it in a central location accessible to everyone
in the hope they will save users time when setting up to use GPSC.

!!! info
    Initially written with [StackEdit](https://stackedit.io/).
