# 

## Accessing GPSC1 and GPSCC2
<!--Depending on how one connects to GPSC1, different resource files are needed to setup the environment. -->
This page describes the process of accessing the clusters and setting up the environment for NRC Metrology users on the Government of Canada General Purpose Scientific Clusters GPSC1 and GPSCC2. 

Available options to connect to GPSC1 are via an [interactive ssh shell](https://portal.science.gc.ca/confluence/display/SCIDOCS/Interactive+Access) (Putty on Windows or ssh from Linux) or using the ThinLinc client to connect to a graphical desktop environment (Kubuntu). The interactive node is [gpsc-in.science.gc.ca](https://portal.science.gc.ca/confluence/display/SCIDOCS/gpsc-in), while the graphical desktop landing point is [gpsc-vis.science.gc.ca](https://portal.science.gc.ca/confluence/display/SCIDOCS/gpsc-vis).

Connection to GPSCC2 is currently only possible via an [interactive ssh shell](https://portal.science.gc.ca/confluence/display/SCIDOCS/Interactive+Access) (Putty on Windows or ssh from Linux) . The interactive node is [gpscc-in.collab.science.gc.ca](https://portal.science.gc.ca/confluence/display/SCIDOCS/gpscc-in).

!!! note 
    Users must have received their login credentials for GPSC1 to be able to connect using any of the options above!

## Setting up the environment

According to SSC, the [preferred way to setup the shell environment](https://portal.science.gc.ca/confluence/display/SCIDOCS/Setting+Up+the+Environment#SettingUptheEnvironment-.profilevs.bashrc) is via a _.profile_ file. In our experience this works very well when loging in interactively to `gpsc-in.science.gc.ca`. However, when connecting to a graphical session (**ThinLinc**), only environment variables defined in *.profile* are properly set, but no *aliases* nor *bash shell functions* are available on the non-login consoles.

To set up the environment of non-login shells one requires a *.bashrc* file. This combination of *.profile* and *.bashrc* works as expected **only** when connecting to a graphical session. However, if one wants to be able to connect interactively and graphically, things get a bit more complicated. If the *.bashrc* file does not have all the settings required by the system ([*ordenv*](https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide) for instance) then the system freezes during login to an interactive shell since it tries .bashrc first.

One could make *.profile* and *.bashrc* identical to circumvent the issue above. But then one would be trying to run *ordenv* scripts twice, which is caught and reported as a warning. This is avoided by adding an IF-statement checking whether *ordenv* has already been setup in both files. 

!!! tip
    A much _leaner_ solution is to just use a *.bashrc* file.

## .profile
Used when connecting via `ssh`. This file is loaded for **login shells** only. Here is an example for user _emh035_:
```bash
#Personal settings
export PATH=\$HOME/bin:\$PATH
source ~/.alias
#Set proper time zone
export TZ=America/Toronto
###############################
# EGSnrc environment settings #
###############################
export EGS_HOME=/home/emh035/EGSnrc/egs_home/
export EGS_CONFIG=/home/emh035/EGSnrc/HEN_HOUSE/specs/dorval.conf
source /home/emh035/EGSnrc/HEN_HOUSE/scripts/egsnrc_bashrc_additions
#############################
#  GPSC Environment settings #
############################# 
if [ "${ORDENV_SETUP}" != 1 ]; then
   export ORDENV_SITE_PROFILE=20190814
   export ORDENV_COMM_PROFILE=
   export ORDENV_GROUP_PROFILE=nrc/profile
   . /fs/ssm/main/env/ordenv-boot-20190814.sh
fi
```

!!! tip "Setup Grid Engine environment (ordenv 4)"
    `ordenv` provides a way for administrators and users to set up a user's working environment according to various group, user, session, and system criteria.

## .bashrc
When connecting via a Windows manager to `gpsc-vis.science.gc.ca` using the ThinLinc client only the environment variables from `.profile` are set when one opens a terminal. But *aliases* and *functions* are not. If one copies those to a `.bashrc` file, these settings are available for each terminal. In principle one could make `.profile` and `.bashrc` identical, but just using *.bashrc* should suffice.
