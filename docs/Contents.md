# Table of Contents
1. [The GPSC1 and GPSCC2 clusters](docs/Introduction.md)
2. [Environment](docs/Environment.md)
3. [Job submission](docs/Jobsubmission.md)
4. [My initial steps](docs/Diary.md)

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTQxMzUxMTEwNV19
-->
