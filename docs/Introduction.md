# The GPSC1 and GPSCC2 clusters
!!! caution
    Some of the links below are only available on the corporate (green) or research (orange) networks.

The [Government of Canada scientific clusters](https://www.canada.ca/en/shared-services/corporate/data-centre-consolidation/high-performance-computing.html) are managed by Shared Services Canada (SSC).  These clusters are integrated into a high performance computing (HPC) environment offering services to internal users (GPSC1, GPSC2) as well as external collaborators (GPSCC2) . 

The [General Purpose Scientific Cluster 1](https://portal.science.gc.ca/confluence/display/SCIDOCS/gpsc1) (GPSC1) supports the scientific computing needs of [several government departments](https://portal.science.gc.ca/confluence/display/SCIDOCS/Partner%20Information) including NRC who has [3200 cores allocated](https://portal.science.gc.ca/confluence/display/PSNRC/gpsc+-+Gridengine+-+Allocations) to be shared by several NRC [projects](https://portal.science.gc.ca/confluence/display/PSNRC/gpsc+-+Gridengine+-+Projects).  This cluster has a total of 4096 regular compute nodes (Xeon E5 2650v2). It also offers 400 large (Xeon E5 4650v2) and 576 superlarge (Xeon E7 8860v3) compute nodes and about two hundred GPU cores (K80 and K100). GPSC1 can only be accessed through the corporate (green) network or the orange (research) network.

In practice each NRC Metrology user has access to 800 cores on GPSC1. Requesting more cores places those jobs on the queue until one of the first 800 finishes. At IRS two independent users can run 800 jobs separately.

The [General Purpose Science Cluster for Collaboration](https://portal.science.gc.ca/confluence/display/SCIDOCS/gpscc2) (GPSCC2) is intended for projects with external collaborators without access to the green or orange networks. Its architecture is almost identical to GPSC1 offering a total of 4096 compute cores, although availability for an individual user is significantly less than that. GPSCC2 can be accessed from the outside and from the black legacy network.

In practice each NRC Metrology user has access to 1600 cores on GPSCC2. Requesting more cores places those jobs on the queue until one of the first 1600 finishes. At IRS two independent users can run 1600 jobs separately. Initially I was able to run on 3952 cores until something changed and the limit changed. 

!!! tip
    GPSCC2 is typically not as busy as GPSC1!!!
