# Table of contents
 - [GE batch-queuing system](#Grid-Engine-batch-queuing-system)
 - [EGSnrc run control](#egsnrc-run-control)
 - [Run control for SGE](#run-control-for-sge)
   * [Input examples](#input-block-syntax) 
 - [Job submission for SGE](#job-submission-for-sge)
   * [_egs-jobsub-xargs_ source code](#egs-jobsub-xargs-source-code) 
 
## Grid Engine batch-queuing system
These clusters use a version of the [Grid Engine](https://portal.science.gc.ca/confluence/display/SCIDOCS/Gridengine) (**GE**) grid computing cluster software system (a.k.a batch-queuing system) for executing Unix-like  batch  jobs  (shell scripts or binaries) on a pool of cooperating CPUs. Grid Engine was first distributed by Genias Software and from 1999, after a company merger, by Gridware, Inc. In 2000, [Sun Microsystems](https://en.wikipedia.org/wiki/Sun_Microsystems "Sun Microsystems") acquired Gridware improving and supporting it under the name Sun Grid Engine (**SGE**). In 2010, [Oracle Corporation](https://en.wikipedia.org/wiki/Oracle_Corporation "Oracle Corporation") acquired Sun and thus renamed **SGE** to [Oracle Grid Engine](https://en.wikipedia.org/wiki/Oracle_Grid_Engine "Oracle Grid Engine"). On October 22, 2013 [Univa](https://en.wikipedia.org/wiki/Univa "Univa") announced that it had acquired Oracle Grid Engine assets and intellectual property making it the sole commercial provider of Grid Engine software under the name [Univa Grid Engine](https://en.wikipedia.org/wiki/Univa_Grid_Engine).

The Grid Engine community started the _Open Grid Scheduler_ and the _Son of Grid Engine_ projects to continue to develop and maintain a free implementation of Grid Engine. The [Son of Grid Engine](https://arc.liv.ac.uk/trac/SGE) project chose its name among other things to maintain the same abbreviation **SGE** and is the version used by SSC for its HPC environment. The [Science Network Documentation](https://portal.science.gc.ca/confluence/display/SCIDOCS/Home) has a very brief introductory [documentation to using Gridengine](https://portal.science.gc.ca/confluence/display/SCIDOCS/Using+Gridengine). 

## EGSnrc run control
The default approach for job submission used by EGSnrc relies on a locking file mechanism whereby the number of histories to be run, the number of histories already executed and the number of running jobs are kept in a job control file (JCF) which has to be locked while being accessed by one of the running jobs. This [run control object](https://nrc-cnrc.github.io/EGSnrc/doc/pirs898/classEGS__RunControl.html#details) (RCO) type is implemented in a [JCF class](https://nrc-cnrc.github.io/EGSnrc/doc/pirs898/classEGS__JCFControl.html#details). This class was introduced in a time when the NRC _computer cluster_ was composed of two very different types of CPUs. Simply distributing histories equally among all CPUs caused the total time of a simulation to be determined by the slowest goup of CPUs. As a consequence, jobs on the slower machines would continue running for a longer time than the jobs on the faster macines. Those CPUs that had finished earlier would seat idle, wasting resources and time. 

The locking file mechanism solved this _bottle-neck_ by balancing the number of histories to be run based on CPU speed. Each job would take smaller chunks of the histories by accessing the locking file until all hisotries had been dispatched. This way, faster CPUs would execute a larger portion of the simulation and the jobs were finished in about the same amount of time.

One _drawback_ of this approach is that ___jobs attempting to access the lock file abort if they have to wait too long___. This could happen in cases where many jobs are finishing at the same time, executing too fast, or writting too much data to their _egsdat_ file. Another _drawback_ is that the first job **must** start before all other jobs since it is the one that creates the JCF. 

### A simple run control class
An alternative to the JCF class is the [simple RCO type](https://nrc-cnrc.github.io/EGSnrc/doc/pirs898/classEGS__RunControl.html#details) designed to split the simulation equally among all parallel jobs.  The last job is in charge of combining all parallel jobs into the final result. 

The only issue with using this RCO is that if the last job ___would___ finish before other jobs, those jobs would not be combined into the final result.

!!! tip
    The simple RCO can replace the JCF RCO if this condition is met at an HPC centre.

### Issues with the simple and JCF RCOs
The locking file mechanism as tested in the **SGE** environment, produces unexpected results. Simulations finish very fast and the final result is based on a significantly lower number of histories. The reason for this is _not well understood_, but it would seem that the implementation of the locking mechanism does not work properly. For instance, if the JCF is not created, then all jobs will run only a small initial chunk of the histories. 

Another issue observed early on during the testing phase of the **SGE** environment was that jobs would not start nor finish synchronously as they were placed on the queue. Since both RCO types rely on the jobs being executed synchronously, i.e., the first job starts first and the last job is expected to finish last, this would cause the combined final result to miss some of the jobs. 

This later issue could be related to the existence of slightly different CPU architectures in a cluster. 

!!! note
    The simple and the JCF RCOs rely on the last job finishing last.

## Run control for SGE
Given the above issues, a new RCO has been developed that does not rely on a JCF, nor it is constrained to the last job for combining all parallel jobs into a final result. However if one has no access to this new implementation, _a temporary alternative is to use the simple RCO_. In this case some jobs might be missed when the last job combines the partial results, but this could be remediated by manually combining all parallel jobs. The next section explains how to use a simple RCO.

### Using a simple RCO
While the new RCO implementation is added to the development branch one could use the option to use a simple RCO:

- Add -s or --simple-run to the end of the [command for submitting SGE jobs](#job-submission-for-sge)

!!! example
    ejx app inp pegs4 p=N -s

Copy the [source code of the job submission script](#egs-jobsub-xargs-source-code) to a known location for instance `$HEN_HOUSE/scripts` and either create an alias to it or append its location to the  `$PATH` environment variable.

!!! attention
    _User will have to combine the results at the end_

### The uniform run control (URC) class
To resolve the above issues a uniform run control (URC) class was developed as alternative to the above RCO types. It divides the number of histories equally among all parallel jobs like the simple RCO, but allows choosing which job(s) will be tasked with combining the results from parallel runs upon their completion. To this end this URC class introduces _watcher jobs_ which check whether jobs have finished and if that is the case, combine them. By default, the last job is taken as the watcher job, but users can define an arbitrary number of them. 

#### Input block syntax
The syntax to define an UCR RCO is shown in the example input block below: 
```yaml
:start run control:
   rco type = uniform           # Or simple, balanced
   check jobs completed = yes   # Defaults to 'yes'
   watcher jobs = j1, j2,...,jn # Defaults to last job
   interval wait time = t_ms    # Defaults to 1000 ms
   number of intervals = N_int  # Defaults to 5
   # Some common options for any RCO
   ncase = 20000000000
   calculation = first # analyze, combine
:stop run control:
```

#### Watcher mode
Once a _watcher job_ has finished, it switches to _watcher mode_ and checks for finished jobs an user-defined number of times (_check_intervals_) spaced in time by an user-defined time interval (_milliseconds_). The check for job completion is based on the existance of _file_name_w*.egsdat_ files in the application directory, where _file_name_ is the input or output file name without extension. For this reason existing _file_name_w*egsdat_ files **_are deleted_** at the begining of the simulation.

At each check interval, jobs that have finished are combined into a partial result. If no other job has finished at the next check, no action is taken and the _watcher job_ sleeps until the next check.

# RCO type

This implementation allows to choose the RCO type via input file. A  job control file [(JCF)](https://nrc-cnrc.github.io/EGSnrc/doc/pirs898/classEGS__JCFControl.html#details) RCO is defined with the RCO type `balanced` and a simple RCO with the keyword `simple`. To use an URCO one must define the RCO type to be of type `uniform`. 

By default, the last job checks 5 times for finished jobs every second for a total time of 5 s:

```yaml
:start run control:
   rco type = uniform           # Or simple, balanced
   # Some common options for any RCO
   ncase = 20000000000
   calculation = first # analyze, combine
:stop run control:
```

In the example above, jobs finishing 5 s or later after the the last job finished are not combined. A manual combination can be requested by changing the `calculation` input key to _combine_ in the `run control` input block.
Or alternatively, the ckeck time interval increased.

!!! tip 
    If `check jobs completed` is set to `no` the URC RCO is _identical_ to the simple RCO!

The `run control` input block below defines an URC RCO with the last job checking after it has finished for other jobs every 1 min for a total of 1 h:
```yaml
:start run control:
   rco type = uniform # simple, balanced
   interval wait time = 60000 # 1 min in ms
   number of intervals = 60   # 1 hr total time
   ncase = 20000000000
   calculation = first # analyze, combine
:stop run control:
```
!!! note 
    If all jobs finish before the hour, the watcher job recombines them at the next check cycle.

## Job submission for SGE

The current script for submitting **EGSnrc** jobs to the **SGE**, _egs-jobsub-xargs_, is a modified version of the EGSnrc job submission script _run_user_code_batch_ that generates **SGE** job request scripts _on-the-fly_ passed to Gridengine's command _jobsub_. After investigating several approaches, the choice was made to use the tool _xargs_ to submit EGSnrc jobs to the cores of available machines (slots).

For simplicity one could use the alias ejx for  _egs-jobsub-xargs_. Typically the command to submit a calculation for the SGE would look like:

```sh
ejx app inp_file pegs_file p=njobs cpu_t=3600
```
The argument `cpu_t` is the CPU time requested to run job to completion and sets the hard run time variable `h_rt` . The time format expected is  `[hh:[mm:]]ss` where hh and mm are optional. For instance the above example requests a CPU time of 3600 s, 60 min or 1 h. Alternatively cpu_t could have been set to:
```
cpu_t=1:00:00
```
The script _egs-jobsub-xargs_  will generate an SGE job script to submit by default 16 EGSnrc jobs on each slot. It will loop through as many slots (machines) as needed to start the requested number of EGSnrc jobs. For efficient use of resources one should make the number of EGSnrc jobs a multiple of 16 or there will be unused cores in the last slot. _This approach guarantees that EGSnrc jobs in each slot and in most occasions that the SGE jobs on different slots are started synchronously_.

### egs-jobsub-xargs source code

The source code of _egs-jobsub-xargs_ is reproduced below:

```bash
#! /bin/bash
###############################################################################
#
#  Script to submit jobs to the Grid Engine job scheduler using xargs
#  Copyright (C) 2018 National Research Council Canada
#
#  This file is part of EGSnrc.
#
#  EGSnrc is free software: you can redistribute it and/or modify it under
#  the terms of the GNU Affero General Public License as published by the
#  Free Software Foundation, either version 3 of the License, or (at your
#  option) any later version.
#
#  EGSnrc is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
#  more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with EGSnrc. If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
#  Author:          Ernesto Mainegra-Hing, 2018
#
#  Contributors:    Iwan Kawrakow
#                   Dave Rogers
#                   Frederic Tessier
#                   Blake Walters
#
###############################################################################
#
# Check enough arguments provided
#
my_name=`echo "$0" |sed 's,.*[\\/],,'`
if test $# -lt 3; then
    cat >&2 <<EOF

Usage: $my_name app inp pegs|pegsless [p=N] [config=xxx] [eh=xxx] [hh=xxx] [cpu_t=t] 
                [email=xxx] [slot=# of slots] [cpus=# of cores] [ram=MB] [tmpfs=MB]
   
app    => EGSnrc app name
inp    => input file (use two double quotes if there is no input file needed) 
pegs   => pegs4 data file.
config => use a configuration different from EGS_CONFIG
p      => number of parallel jobs
cpu_t  => CPU time requested to run job to completion
slot   => slots from the dev parallel environment (PE)
cpus   => cores requested per slot
ram    => RAM memory in MB per job
tmpfs  => Temporary file space in MB per job

EOF
    exit 1
fi
#
# Initialization
#
app=$1
inp=$2
pegs=$3
egs_home="$EGS_HOME"
hen_house_arg=
start_job=0
stop_job=0
egs_configuration="$EGS_CONFIG"
n_parallel=0
other_args=
testing=no
#########################
# NRC specific settings #
#########################
# Project name
project=nrc_mss
# email
email=ernesto.mainegra-hing@nrc-cnrc.gc.ca
#image=nrc/nrc_all_default_ubuntu-14.04-amd64_latest
image=nrc/nrc_all_default_centos-7-amd64_latest
#########
# CPU time requested to run job to completion
# Sets the hard run time variable h_rt
#   format: [hh:[mm:]]ss <- hh and mm are optional
#########
cpu_t=7200
# slots from the dev parallel environment (PE)
slot=1
# cpus requested per slot
cpus=16 #32
# RAM memory in MB per job
ram=4000
# Temporary file space in MB per job
tmpfs=2000
simple=no
rmegsdat=yes

# First 3 arguments already taken above
shift 3
#
# Parse command line for arguments
#
while test $# -gt 0; do

    case "$1" in

        proj=*)  project=`echo $1 | sed 's/proj=//'` ;;
          eh=*)  egs_home=`echo $1 | sed 's/eh=//'` ;;
          hh=*)  hen_house_arg=`echo $1 | sed 's/hh=//'` ;;
      config=*)  egs_configuration=`echo $1 | sed 's/config=//'` ;;
           p=*)  n_parallel=`echo $1 | sed 's/p=//'` ;;
       start=*)  start_job=`echo $1 | sed 's/start=//'`;;
        stop=*)  stop_job=`echo $1 | sed 's/stop=//'`;;
       cpu_t=*)  cpu_t=`echo $1 | sed 's/cpu_t=//'`;;
        cpus=*)  cpus=`echo $1 | sed 's/cpus=//'`;;
        slot=*)  slot=`echo $1 | sed 's/slot=//'`;;
         ram=*)  ram=`echo $1 | sed 's/ram=//'`;;
       tmpfs=*)  tmpfs=`echo $1 | sed 's/tmpfs=//'`;;
       email=*)  email=`echo $1 | sed 's/email=//'`;;
#      simple=*)  simple=`echo $1 | sed 's/simple=//'` ;;
    rmegsdat=*)  rmegsdat=`echo $1 | sed 's/rmegsdat=//'` ;;
     testing=*)  testing=`echo $1 | sed 's/testing=//'` ;;
             *) # concatenate everything else to other_args
                # and hope that the user knows what they are doing.
                if test "x$other_args" = x; then
                    other_args="$1"
                else
                    other_args="$other_args $1"
                fi
                ;;
    esac
    shift
done
#
# Check that EGS_HOME is set
#
if test "x$egs_home" = x; then

    if test "x$EGS_HOME" = x; then
       cat >&2 <<EOF

The environment variable EGS_HOME is not set. You have to either
define EGS_HOME to point to a valid location or you must run this
script as
    $my_name ... eh=path_to_work_area

EOF
       exit 1
    fi
    cat >&2 <<EOF

There should be no spaces between 'eh=' and the work area path!

EOF
    exit 1
fi
#
# Check that EGS_CONFIG is set
#
if test "x$egs_configuration" = x; then

    if test "x$EGS_CONFIG" = x; then
        cat >&2 <<EOF

The environment variable 'EGS_CONFIG' is not set. You have to either
define EGS_CONFIG to point to a valid EGSnrc config file or
you must run this script as
    $my_name ... config=some_config_file

EOF
        exit 1
    fi
    cat >&2 <<EOF

There should be no spaces between 'config=' and the name of the configuration
file!

EOF
    exit 1
fi
#
# Check that EGS_CONFIG points to an existing file
#
if test ! -f "$egs_configuration"; then

    cat >&2 <<EOF

The file $egs_configuration does not exist. The environment variable
EGS_CONFIG or the config=some_config command line option must
define the absolute path to a valid EGSnrc config file.

EOF
    exit 1
fi
#
# Check that HEN_HOUSE is set
#
if test "x$hen_house_arg" = x; then
    if test "x$HEN_HOUSE" = x; then
        hen_house=`cat $egs_configuration | grep "HEN_HOUSE =" | sed 's/HEN_HOUSE = //'`
    else
        hen_house="$HEN_HOUSE"
    fi
else
    hen_house=$hen_house_arg
fi

my_machine=`cat $egs_configuration | grep "my_machine =" | sed 's/my_machine = //'`

if test ! -d "$egs_home"; then

    echo "The directory '$egs_home' dose not exist. Creating it." >&2
    if { mkdir "$egs_home"; status=$?; (exit $status); }; then
       :
    else
       echo "Failed." >&2
       exit 1
    fi

fi

if test ! -d "$egs_home/$app"; then

    echo "The directory '$egs_home/$app' dose not exist. Creating it." >&2
    if { mkdir "$egs_home/$app"; status=$?; (exit $status); }; then
       :
    else
       echo "Failed." >&2
       exit 1
    fi

fi

executable="${egs_home}bin/$my_machine/$app"
#
# Check if executable exists
#
if test ! -x $executable; then
    executable="${hen_house}bin/$my_machine/$app"
    if test ! -x $executable; then
        echo "No $app executable on your area or on HEN_HOUSE" >&2
        exit 1
    fi
fi
#
# Check if running pegslessly
#
if test "$pegs" = pegsless; then
    command="$executable -b"
else
    command="$executable -p $pegs -b"
fi

#
# Add input file to command
#
if test "x$inp" != x; then
    command="$command -i $inp"
fi

#
# Add EGSnrc environment to command
#
command="$command -e $egs_home -H $hen_house"

#
# Add rest of arguments to command
#
if test "x$other_args" != x; then
    command="$command -i $other_args"
fi
#######################################
# NOT NEEDED! 
# In new implementation, RCO type is 
# requested via input file!
# Left here in case a user does not
# have the UCRO implementation yet.
######################################
# This goes in the extra arguments.
# Check if simple RCO desired
######################################
if test "x$simple" = xyes; then
   command="$command -s"
fi

# Remove previous egsdat files
if test "x$rmegsdat" = xyes; then
   rm -f ${egs_home}${app}/${inpf}_w*.egsdat
fi

if test $start_job -ge 0; then
    if test $stop_job -gt $start_job; then
        if test $n_parallel -eq 0; then
            n_parallel=`expr $stop_job - $start_job`
        fi
    else
        stop_job=`expr $start_job + $n_parallel`
    fi
fi

inpf=`echo $inp |sed 's/.egsinp//'`
the_email=dummy@nowhere.org
#
# Check if running multiple jobs
#
if test $n_parallel -gt 0; then
  command="$command -P $n_parallel"
  job=$start_job
  first_job=`expr $start_job + 1`
  # Submit multiple jobs
  while true; do
    job=`expr $job + 1`
    # Exit loop condition
    if test $job -gt $stop_job; then break; fi
    # Only send email about last job
    jobs_left=`expr $stop_job - $job`
    if test $jobs_left -le $cpus; then the_email=$email; fi
    #if test $job -eq $stop_job; then the_email=$email; fi
    the_command="$command -j $job -f $first_job"
    # Set chunk of tasks to run per node (per slot)
    top_job=`expr $job + $cpus - 1`
    if test $top_job -gt $stop_job; then top_job=$stop_job; fi
    name=${inpf}_w${job}-${top_job}
    if test "x$testing" = xyes; then
      cat > ${name}.sh << EOF
#!/bin/bash
#$ -S /bin/bash
#$ -N $name        # Modify job name as needed.
#$ -M $the_email   # NOTE: Replace with appropriate email address
#$ -P $project
#$ -m bea
#$ -j y
#$ -o ${egs_home}${app}/$name.out
#$ -pe dev ${slot}
#$ -l res_cpus=${cpus}
#$ -l res_mem=${ram}
#$ -l res_tmpfs=${tmpfs}
#$ -l res_image=${image}
#$ -l h_rt=${cpu_t}
# Export these environmental variables
#$ -v EGS_CONFIG=$egs_configuration
#$ -v HEN_HOUSE=$hen_house
#$ -v EGS_HOME=$egs_home
#$ -v TZ=America/Toronto
# Call your program
for i in \$(seq $job $top_job);do
    echo \$i;
done | xargs -P${cpus} -n1 -I{} sh -c '$command -j \$1 -f $first_job'  - {}
EOF
    else
      cat << EOF | jobsub -
#!/bin/bash
#$ -S /bin/bash
#$ -N $name          # Modify job name as needed.
#$ -M $the_email   # NOTE: Replace with appropriate email address
#$ -P $project
#$ -m bea
#$ -j y
#$ -o ${egs_home}${app}/$name.out
#$ -pe dev ${slot}
#$ -l res_cpus=${cpus}
#$ -l res_mem=${ram}
#$ -l res_tmpfs=${tmpfs}
#$ -l res_image=${image}
#$ -l h_rt=${cpu_t}
# Export these environmental variables
#$ -v EGS_CONFIG=$egs_configuration
#$ -v HEN_HOUSE=$hen_house
#$ -v EGS_HOME=$egs_home
#$ -v TZ=America/Toronto
# Call your program
for i in \$(seq $job $top_job);do
    echo \$i;
done | xargs -P${cpus} -n1 -I{} sh -c '$command -j \$1 -f $first_job'  - {}
EOF
    fi
    job=$top_job
  done
#
# Submitting one job
#
else
  name=$inpf
    if test "x$testing" = xyes; then
        echo "Job $name: Executing $command using jobsub"
        echo "qsub options: CPU time=$cpu_t email=$email output=${egs_home}${app}/$name.out"
    else
        cat << EOF | jobsub -
#! /bin/bash
#
## hello.job
#
#$ -N $name             # Modify job name as needed.
#
#$ -M $email          # NOTE: Replace with appropriate email address
#$ -P $project
#$ -m bea
#
#$ -j y
#$ -o ${egs_home}/${app}$name.out
#
#$ -pe dev ${slot}
#$ -l res_cpus=${cpus}
#$ -l res_mem=${ram}
#$ -l res_tmpfs=${tmpfs}
#
#$ -l res_image=${image}
#
#$ -l h_rt=${cpu_t}
#
# Export these environmental variables
#$ -v EGS_CONFIG=$egs_configuration
#$ -v HEN_HOUSE=$hen_house
#$ -v EGS_HOME=$egs_home
#$ -v TZ=America/Toronto

 # Call your program

$command
EOF
    fi
fi

exit 0
```
