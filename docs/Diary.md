# What is this? 

These unredacted notes reflect my journey through the GPSC1 and the rather scattered information on the government of Canada science portal. I have placed them here to keep it in a central location and hopefully save time for future users. Information from here has bee used to complete the pages of this documentation.

## UPDATE May 31st,2019

- Job scheduling has improved considerably and now jobs start immediately. Had to tweak the resource allocation in the submission script. It ALWAYS wants to have a minimum of 2Gb for temporary storage and RAM.

- Current limitation in number of cores (EGSnrc jobs, aka egs-jobs) is 800 for GPSC1. I was able to run in 1600 at some point. Not sure if the new implementation of **Fairshare** has anything to do with it. Note that the way the submission script works is by starting 16 egs-jobs on each machine/slot, this is equivalent to one GE-job. Which means we are being capped at either 50 GE-jobs (CPUs) or 800 egs-jobs (cores).

- Another limitation in the GE submission system is that input files cannot start with a digit nor can they contain special characters such as @.


Usage:

- Accessing GPSC1: dorval alias to ssh .....

- job submission:
```
    egs-xargs app inp pegs|pegsless p=cores cpu_t=[hh:mm:ss] [other-options]
              egs-xargs: alias to $HEN_HOUSE/scripts/egs-jobsub-xargs
              options: see script $HEN_HOUSE/scripts/egs-jobsub-xargs
```

- Checking the queue:
```
  jobst: queue status
  jobs: alias to 'jobst -u emh035'
```
- input file uniform RCO syntax: see `egs_run_control.cpp` for details

   
Example
```yaml
:start run control:
   ncase = 1.6e+12
   rco type = uniform
   interval wait time = 60000 # 1 min in ms
   number of intervals = 60   # 1 hr total time
   check jobs completed = yes
   #calculation = combine
:stop run control:
```
## INITIAL STEPS

### Accessing GPSC:

- ON your orange machine create `.ssh/config`
```
[mainegrahe@nrc-014925r .ssh]$ cat config 
ForwardX11 yes
Host *.science.gc.ca
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
```
- ssh to GPSC:

  ssh emh035@gpsc-in.science.gc.ca

  One can setup password-less ssh-ing via the usual method of generating a public rsa key on your orange machine and appending it to your $HOME/.ssh/authorized_keys  on the GPSC1. This allows issuing commands from orange directly to GPSC1.

!!! note 
    Still don't know how to set things up to be able to work on orange with my GPSC1 home area.
    Maybe a simple nfs mount? Asked John Marshall in Dorval, wainting for an answer!

- Setup Grid Engine environment (ordenv 4): `ordenv` provides a way for administrators and users to set up a user's working environment according to various group, user, session, and system criteria.
```
   bash-4.1$ cat .profile
   export ORDENV_SITE_PROFILE=20180528
   export ORDENV_COMM_PROFILE=
   export ORDENV_GROUP_PROFILE=
   . /fs/ssm/main/env/ordenv-boot-20180430.sh
```
   More info at the [Setting Up the Environment](https://portal.science.gc.ca/confluence/display/SCIDOCS/Getting+Started+-+Setting+Up+the+Environment) section of the [GC confluence](https://portal.science.gc.ca/confluence/display/SCIDOCS) website.

- The GPSC uses SGE (Son of Grid Engine) as its job scheduler via the _jobctl_ package which provides a way to interact with a queueing system

## GPSC1 Notes

### Beginnings: 24/09/2018
- Jobs do not start immediately. The order is unpredictable. I just submitted 15 jobs
  that should finish in 5 s and half of them were running forever. Had to stop them.

- It is clearly not a homogeneous CE

- Deleting jobs takes a long time. Nothing happens instantaneously!

- Can this be changed? Priority levels?


### Progress report: 16/10/2018

- Submitting one SGE job per EGSnrc parallel task is costly for GPSC1's scheduler. Changed script
  to submit several tasks on each node (one SGE job) using xargs (egs-jobsub-xargs). Now all tasks
  on the same node start at the same time, and different jobs might be slightly off sync, but start 
  at around the same time.

- Another way is to use array jobs which are not so hard on the scheduler as they are scheduled as one
  SGE job. However, jobs starting time is less predictable. Perhaps one should combine array jobs with
  xargs.

- John Marshall recommended using Workerjobs ...

### Progress report: 25/10/2018

- Still using xargs on each node, have to test if it works on multiple nodes:

- Submitted 32 SGE jobs spawning 32 processes (1024 2-min EGS jobs): 
     
     First 7 SGE jobs started around the same time within 2 minutes but not instantaneoulsy. 
     
     Rest of the jobs sat waiting for a longer time: 24 min delay between first and last SGE jobs 

!!! summary
    Our initial suspicion is confirmed; GPSC works well for long production runs that take hours, 
    but not for shorter runs that take a few minutes during the development phase of a model or testing
    of an algorithm to solve a specific problem.

!!! tip
    Perhaps this can be solved using Workerjobs. Further investigation required.

## URCO implementation
I am currently testing an implementation of a uniform run control object (UCRO) as alternative 
to the use of lock files (JCF: job control file). We are currently exploring the use of EGSnrc 
on an HPC cluster running the Sun Grid Engine batch-queuing system. The UCRO simply divides the number of histories equally among all jobs and combines them at **_the end_**.

The tricky part here is detecting **_the end_**. Depending how your jobs are handled by the scheduler you might end up not having a proper FIFO queue, hence assuming that the last job finishes last is not true. As a consequence,  the last job will only combine those jobs that have already finished, which might no be all the jobs submitted. This is not a big deal as one can rerun the simulation interactively with the calculation option to combine all jobs once they are done. 
But it is definitely a nuissance!

To circumvent this, I have added two levels of control to the URCO:

- Time: User can ask the watcher job (by default the last job) to wait before attempting to combine all results (default set to 5 s). One could select a longer time (in fact very long) to guarantee that all jobs completed.
  This combined with an option to check from time to time if more jobs are finishing and produce intermediate results, should result

- Watcher jobs: One can change which job is the one combining all jobs (watcher), and there can be more than one watcher. This one is particularly useful if for some reason some jobs might fail to start, for instance because there is no enough resources on the cluster. In that case you could not have the last job even starting! The one that almost always start is the first one. But to be sure one could have the first, middle and last job as watcher jobs.

### Using a simple RCO
If one wants to wait until the URCO implementation is added to the development branch, one could use the option to use a simple RCO:

- Add -s or --simple-run to the end of the command for running your jobs, for instance:

  exb app inp pegs4 p=N -s

!!! tip
    You will have to combine the results at the end
