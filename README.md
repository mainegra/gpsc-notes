# The Government of Canada HPC environment

These pages detail the steps taken to set up the Government of Canada 
HPC environment for an NRC Metrology user. It covers the experiences from 
figuring out how to set up the user environment to implementing a job submission 
strategy for the SGE batch-queuing system. To this end it was necessary to comb 
through the rather scattered information on the Government of Canada Science Portal. 

I have placed these notes here to keep it in a central location accessible to 
to everyone in the hope they will save users time when setting up to use GPSC. 

Get started by going to the [documentation page](https://mainegra.gitlab.io/gpsc-notes/)